#!/usr/bin/python

import os
import os.path
import sys
import md5
import subprocess
import datetime
import time
import logging
import base64
import concurrent

import tinys3
import requests

# Number of simultaneous uploads
THREADS = 10

# Maximum queue size - about 20-50% higher than threads so we keep the network
# busy
MAX_QUEUE = THREADS + 5

# Overwrite corrupt files (when the digest doesn't match)? Setting this to true
# is probably a bad idea... the corruption is more likely to be on your end,
# not S3 ;)
OVERWRITE_BAD = True

# The S3 region we should use - set this where your bucket lives
ENDPOINT = 's3-eu-west-1.amazonaws.com'

# Ignore any paths ending in these strings
EXCLUDED_FILES = ('.DS_Store', '.localized', 'desktop.ini')

# The storage class to use for newly created files - use a lifecycle policy to
# move files to Glacier if required.
#
# Pricing for eu-west-1 / month (at 20151122)
#   STANDARD -           $0.0300 / GB = $30.00 / TB
#   STANDARD_IA -        $0.0125 / GB = $12.50 / TB
#   GLACIER -            $0.0070 / GB = $ 7.00 / TB (not available from the API, use STANDARD_IA and then a policy on the bucket)
#   REDUCED_REDUNDANCY - $0.0240 / GB = $24.00 / TB 
STORAGE_CLASS = 'STANDARD_IA'

try:
  base = sys.argv[1]
  bucket = sys.argv[2]
  access_key = sys.argv[3]
  secret_key = sys.argv[4]
except IndexError:
  print "Usage: %s BASEDIR BUCKET ACCESS_KEY SECRET_KEY" % (sys.argv[0])
  raise SystemExit

if not os.path.isdir(base):
  print "Invalid argument: BASEDIR must be a directory."

# Globals
flash_message = ""
in_flight = set()
pool = tinys3.Pool(access_key, secret_key, tls=False, default_bucket=bucket, size=THREADS, endpoint=ENDPOINT)
conn = tinys3.Connection(access_key, secret_key, tls=False, default_bucket=bucket, endpoint=ENDPOINT)

logging.basicConfig(format="%(asctime)s %(name)s %(levelname)s %(message)s")
logger = logging.getLogger('s3sync')
logger.setLevel(logging.DEBUG)

# Get the MD5 hash of file with path f
def f_md5(f):
  m = md5.new()
  with open(f, 'rb') as fp:
    for chunk in iter(lambda: fp.read(1024 * 100), ""):
      m.update(chunk)
    return m

def exclude(path):
  global EXCLUDED_FILES
  return path.endswith(EXCLUDED_FILES)

# Read in exif data, if it exists, and return a dictionary of Amazon-style
# attributes:
# 
#  {
#    'x-amz-meta-exif-samplesperpixel': '1', 
#    'x-amz-meta-exif-flash': 'Off, Did not fire', 
#    'x-amz-meta-exif-exposuremode': 'Auto', 
#    'x-amz-meta-exif-subfiletype': 'Reduced-resolution image', 
#    'x-amz-meta-exif-saturation': 'Normal',
#    ...
#  }
#
def exif_headers(f):
  response = {}
  try:
    output = subprocess.check_output(["/usr/local/bin/exiftool", "-args", "-File:All", "-EXIF:All", "-GPS:All", "--a", f]) 
  except subprocess.CalledProcessError, e:
    # Fail silently
    return {}
  
  desired_keys = [
    'filename',
    'directory',
    'filemodifydate',
    'filetype',
    'mimetype',
    'make',
    'model',
    'orientation',
    'imagewidth',
    'imageheight',
    'compression',
    'copyright',
    'exposuretime',
    'fnumber',
    'exposureprogram',
    'iso',
    'createdate',
    'exposurecompensation',
    'maxaperturevalue',
    'meteringmode',
    'flash',
    'focallength',
    'usercomment',
    'exposuremode',
    'gpslatituderef',
    'gpslatitude',
    'gpslongituderef',
    'gpslongitude',
    'gpsaltituderef',
    'gpsaltitude',
    ]

  for line in output.split('\n'):
    try:
      key, value = line[1:].split('=')
    except ValueError:
      pass

    if key.lower() in desired_keys:
      response['x-amz-meta-exif-' + key.lower()] = value

  return response

# Return a flashed message if something stored one
def flash():
  global flash_message
  if flash_message:
    response = "(%s)" % (flash_message)
    flash_message = ""
    return response
  else:
    return ""

# Print a message on success, retry the file on failure
def callback_done(path, base, r):
  global logger, in_flight, pool
  key = path[len(base)+1:]
  try:
    if r.result().status_code == 200:
      logger.info("%s: complete (%s) %s" % (key, r.result().url, flash()))
      return True
    else:
      logger.warning("%s: FAILED for %s with status %d: %s\n\n%s\n" % (key, r.url, r.status_code, r.text))
      put(path, base, pool, in_flight)
  except requests.ConnectionError as e:
    logger.warning("%s: FAILED with ConnectionError %s" % (key, e))
    put(path, base, pool, in_flight)
  except requests.HTTPError as e:
    logger.warning("%s: FAILED with HTTPError %s" % (key, e))
    put(path, base, pool, in_flight)

  return False

# Upload a file into S3, with headers
def put(path, base, pool, in_flight):
  key = path[len(base)+1:]
  m = f_md5(path)

  headers = exif_headers(path)
  headers['Content-MD5'] = base64.b64encode(m.digest())
  headers['Content-Type'] = headers['x-amz-meta-exif-mimetype'] if 'x-amz-meta-exif-mimetype' in headers.keys() else 'application/octet-stream'
  headers['x-amz-storage-class'] = STORAGE_CLASS
  headers['x-amz-server-side-encryption'] = 'AES256' # SSE is easy and free, let's use it!
  headers['x-amz-meta-md5'] = m.hexdigest()
  headers['x-amz-meta-created'] = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')

  fp = open(path, 'rb')
  r = pool.upload(key, fp, '', headers=headers)
  r.add_done_callback(lambda x: callback_done(path, base, x))
  in_flight.add(r)

# Returns true if the file already exists in S3 with a matching MD5
def exists(path, base):
  global conn 
  key = path[len(base)+1:]

  try:
    for f in conn.list(prefix=key):
      # {'storage_class': 'STANDARD', 'last_modified': datetime.datetime(2015,
      # 11, 22, 19, 49, 7), 'etag': '801541a6c77b94b27ce803c6c5766fbc', 'key':
      # 'test/.DS_Store', 'size': 26628}
      if f['key'] == key: 
        if f['etag'] == f_md5(path).hexdigest():
          logger.info("%s: Exists in S3 with matching digest. Skipping" % (key))
          return True
        else:
          logger.warning("%s: Exists in S3, but digest does not match! %s" % (key , "You have requested I overwrite bad files; adding to upload queue" if OVERWRITE_BAD else "Leaving unchanged"))
          return not OVERWRITE_BAD
  except requests.exceptions.HTTPError, e:
    print e

  return False

# Mainloop
if __name__ == "__main__":
  logger.debug("Using endpoint: %s" % (ENDPOINT))

  for (folder, subfolders, files) in os.walk(base):
    for f in files:
      path = os.path.join(folder, f)
      key = path[len(base)+1:]

      if exclude(path):
        continue
      
      if not exists(path, base):
        put(path, base, pool, in_flight)
        logger.info("%s: Started upload %s" % (key, flash()))

      if len(in_flight) >= MAX_QUEUE:
        flash_message = "throttling with %d uploads queued, %d worker threads" % (len(in_flight), THREADS)
        (done, in_flight) = concurrent.futures.wait(in_flight, return_when=concurrent.futures.FIRST_COMPLETED)


  logger.info("That's it! Waiting for queue to clear...")
  pool.all_completed(in_flight)

  logger.info("Done.")
    

